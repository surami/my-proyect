import { registerRootComponent } from 'expo';

//import App from './App';
//import App from './modulo2/estado3';
//import App from './TodoApp/App';
//import App from './Chapter4/Listing-4.1-UsingInlineStyles/App';
//import App from './chapter4/Listing-4.20-ProfileCard-Step6-Completed/App';
//import App from './chapter5/Listing-5.1-ProfileCard-Step7-DropShadows/App';
// import App from './chapter5/Listing-5.2-Scale/App';
// import App from './chapter5/Listing-5.3-ProfileCard-Step8-Thumbnail/App';
//import App from './chapter5/Listing-5.10-Flexwrap/App';
//import App from './chapter7/AnimatedStagger';
import App from './listing81';

// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in Expo Go or in a native build,
// the environment is set up appropriately
registerRootComponent(App);
